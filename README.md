# FyfyStream - Finance App
[![Deploy](https://github.com/fyfyio/fyfy-finance-app/actions/workflows/gh-pages-prod.yml/badge.svg)](https://github.com/fyfyio/fyfy.finance-app/actions/workflows/gh-pages-prod.yml)

Web application client for the **Fyfy streaming payments protocol**.

Serverless. Realtime. Free and open-source. Built on Solana.

- [Short demo]() 
- [[Extended demo]()]
- [Slides](https://fyfy.finance/public/fyfy_slides.pdf)
- [Website](https://fyfy.finance)
- [App](https://app.fyfy.finance)
- [GitHub](https://github.com/fyfy-finance)
- [Telegram](https://t.me/fyfyio)
- [Twitter](https://twitter.com/fyfyio)

To run locally, git clone the repository and then:
```
  npm i
  npm run start
